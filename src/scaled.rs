use crate::interpolate::Interpolate;

pub fn scaled<T: Copy + Interpolate>(ts: f64, array: &[(f64, T)]) -> T {
    // Find the interval where ts lies

    let (mut prev_ts, mut prev) = array.first().unwrap();

    for (next_ts, next) in array.iter().skip(1) {
        if ts >= prev_ts && ts < *next_ts {
            let amount = ts as f64 - prev_ts as f64;
            let interval = *next_ts as f64 - prev_ts as f64;
            let scale_factor = amount / interval;
            return prev.interpolate(scale_factor, next);
        }
        prev_ts = *next_ts;
        prev = *next;
    }

    // Or we are at the end, so take the last entry
    array.last().unwrap().1
}

pub fn select(ts: f64, array: &[(f64, &str)]) -> String {
    // Find the interval where ts lies

    let (mut prev_ts, mut prev) = array.first().unwrap();

    for (next_ts, next) in array.iter().skip(1) {
        if ts >= prev_ts && ts < *next_ts {
            return prev.to_string();
        }
        prev_ts = *next_ts;
        prev = *next;
    }

    // Or we are at the end, so take the last entry
    array.last().unwrap().1.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scaled_interpolates_linearly() {
        let array = [(0.0, 0.0), (100.0, 100.0), (200.0, 0.0), (300.0, 1000.0)];
        assert_eq!(0.0, scaled(0.0, &array));
        assert_eq!(10.0, scaled(10.0, &array));
        assert_eq!(99.0, scaled(99.0, &array));
        assert_eq!(100.0, scaled(100.0, &array));

        assert_eq!(0.0, scaled(200.0, &array));
        assert_eq!(100.0, scaled(210.0, &array));
        assert_eq!(990.0, scaled(299.0, &array));
        assert_eq!(1000.0, scaled(300.0, &array));
    }

    #[test]
    fn scaled_before_or_after_intervals_gives_last_value() {
        let array = [(100.0, 0.0), (200.0, 100.0)];
        assert_eq!(100.0, scaled(0.0, &array));
        assert_eq!(100.0, scaled(201.0, &array));
    }

    #[test]
    fn select_chooses_the_right_interval() {
        let array = [(0.0, "a"), (100.0, "b"), (200.0, "c")];
        assert_eq!("a", select(0.0, &array));
        assert_eq!("a", select(99.0, &array));
        assert_eq!("b", select(100.0, &array));
        assert_eq!("c", select(201.0, &array));
        assert_eq!("c", select(300.0, &array));
    }
}
