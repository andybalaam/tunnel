use crate::interpolate::Interpolate;

#[derive(Clone, Copy)]
pub struct Color(pub u8, pub u8, pub u8);

impl Color {
    pub fn as_css(&self) -> String {
        format!("rgb({}, {}, {})", self.0, self.1, self.2)
    }
}

impl Interpolate for Color {
    fn interpolate(&self, sc: f64, other: &Color) -> Color {
        Color(
            self.0.interpolate(sc, &other.0),
            self.1.interpolate(sc, &other.1),
            self.2.interpolate(sc, &other.2),
        )
    }
}

#[derive(Clone, Copy)]
pub struct ColorA(pub u8, pub u8, pub u8, pub f64);

impl ColorA {
    pub fn as_css(&self) -> String {
        format!("rgba({}, {}, {}, {})", self.0, self.1, self.2, self.3)
    }
}

impl Interpolate for ColorA {
    fn interpolate(&self, sc: f64, other: &ColorA) -> ColorA {
        ColorA(
            self.0.interpolate(sc, &other.0),
            self.1.interpolate(sc, &other.1),
            self.2.interpolate(sc, &other.2),
            self.3.interpolate(sc, &other.3),
        )
    }
}
