mod color;
mod interpolate;
mod levels;
mod scaled;
mod tunnel;
mod utils;
extern crate rand;

use crate::color::ColorA;
use crate::scaled::{scaled, select};
use crate::tunnel::Tunnel;
use levels::Bonus;
use rand::rngs::StdRng;
use rand::Rng;
use rand::SeedableRng;
use wasm_bindgen::prelude::*;

const MODEL_WIDTH: usize = 401;
const MODEL_HEIGHT: usize = 300;
const SHIP_DIST: f64 = 50.0; // How far from the left is the ship?
const GRAVITY: f64 = 0.00005;
const AIR_RESISTANCE: f64 = 0.0007;
const ROCKET_BOOST: f64 = 0.0002;
const INITIAL_SPEED: f64 = 0.02;
const FLAME_EVERY: u32 = 10;
const BONUS_RADIUS: f64 = 10.0;

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, PartialEq)]
pub enum Mode {
    Intro = 0,
    Running = 1,
    Dead = 2,
    ReenteringIntro = 3,
}

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub struct Particle {
    pub x: f64,
    pub y: f64,
    pub vx: f64,
    pub vy: f64,
    t: u32,
}

pub const FIRE_ALPHA: [(f64, ColorA); 3] = [
    (0.0, ColorA(255, 255, 0, 1.0)),
    (200.0, ColorA(255, 0, 0, 1.0)),
    (1000.0, ColorA(128, 128, 128, 0.0)),
];

#[wasm_bindgen]
impl Particle {
    pub fn color(&self) -> String {
        scaled(self.t as f64, &FIRE_ALPHA).as_css()
    }

    fn tick(&mut self, ts_delta: u32, speed_up: f64) {
        self.t += ts_delta;
        self.x += ts_delta as f64 * self.vx * speed_up;
        self.y += ts_delta as f64 * self.vy * speed_up;

        self.vy += ts_delta as f64 * GRAVITY;
        self.vy -= ts_delta as f64 * self.vy * AIR_RESISTANCE;
        self.vx -= ts_delta as f64 * self.vx * AIR_RESISTANCE;
    }

    fn alive(&self) -> bool {
        self.t < 1000 && self.y < MODEL_HEIGHT as f64
    }
}

#[wasm_bindgen]
pub struct Model {
    rng: StdRng,
    pub width: usize,
    pub height: usize,
    pub ship_dist: f64,
    pub ts: u32,
    pub x: f64,
    pub y: f64,
    pub vy: f64,
    last_flame_ts: u32,
    pub mode: Mode,
    pub high_score: u32,
    pub dead_ts: u32,
    tunnel: Tunnel,
    particles: Vec<Particle>,
    bonuses: Vec<Bonus>,
    bonus_score: u32,
    bonus_collected_at: f64,
}

#[wasm_bindgen]
impl Model {
    pub fn new(seed: u32) -> Model {
        utils::set_panic_hook();
        let middle = MODEL_HEIGHT as f64 / 2.0;

        Model {
            rng: StdRng::seed_from_u64(123456),
            width: MODEL_WIDTH,
            height: MODEL_HEIGHT,
            ship_dist: SHIP_DIST,
            ts: 0,
            x: SHIP_DIST,
            y: middle,
            vy: -0.02,
            last_flame_ts: 0,
            mode: Mode::Intro,
            high_score: 0,
            dead_ts: 0,
            tunnel: Tunnel::new(seed, MODEL_WIDTH, MODEL_HEIGHT, SHIP_DIST),
            particles: Vec::new(),
            bonuses: levels::bonuses(),
            bonus_score: 0,
            bonus_collected_at: -1000.0,
        }
    }

    pub fn tick(&mut self, ts: u32, power: bool) {
        let ts_delta = ts - self.ts;
        let speed_up = self.speed_up();
        self.ts = ts;
        self.tick_particles(ts_delta, speed_up);
        match self.mode {
            Mode::Intro => self.tick_intro(ts_delta, power),
            Mode::Running => self.tick_running(ts_delta, power, speed_up),
            Mode::Dead => self.tick_dead(ts_delta, power, speed_up),
            Mode::ReenteringIntro => self.tick_intro(ts_delta, power),
        }
    }

    fn tick_particles(&mut self, ts_delta: u32, speed_up: f64) {
        for p in self.particles.iter_mut() {
            p.tick(ts_delta, speed_up);
        }
        self.particles.retain(|&p| p.alive())
    }

    fn tick_dead(&mut self, ts_delta: u32, power: bool, speed_up: f64) {
        self.tick_particles(ts_delta, speed_up);
        if self.ts > self.dead_ts + 1000 && power {
            let score = self.score();
            if score > self.high_score {
                self.high_score = score;
            }
            self.mode = Mode::ReenteringIntro;
            self.reset();
        }
    }

    fn tick_intro(&mut self, ts_delta: u32, power: bool) {
        self.x += ts_delta as f64 * INITIAL_SPEED;
        if self.x > 4000.0 {
            self.x = 0.0;
        }
        self.fill_in_heights(self.half_gap());
        if self.mode == Mode::ReenteringIntro {
            if self.x > 65.0 {
                self.mode = Mode::Intro;
            }
        } else if power {
            self.mode = Mode::Running;
            self.reset();
        }
    }

    fn reset(&mut self) {
        self.x = SHIP_DIST;
        self.y = MODEL_HEIGHT as f64 / 2.0;
        self.vy = -0.02;
        self.tunnel.reset();
        self.fill_in_heights(self.half_gap());
        self.bonus_score = 0;
        self.bonus_collected_at = -1000.0;
    }

    fn tick_running(&mut self, ts_delta: u32, power: bool, speed_up: f64) {
        self.x += ts_delta as f64 * INITIAL_SPEED * speed_up;
        self.y += ts_delta as f64 * self.vy * speed_up;

        self.vy += ts_delta as f64 * GRAVITY;
        self.vy -= ts_delta as f64 * self.vy * AIR_RESISTANCE;
        if power {
            self.vy -= ts_delta as f64 * ROCKET_BOOST;
            if self.ts - self.last_flame_ts > FLAME_EVERY {
                for _ in 0..(1 + (20 / ts_delta)) {
                    let xd = self.rng.gen_range(-2.0, 2.0);
                    self.particles.push(Particle {
                        x: self.x + xd,
                        y: self.y + 3.0,
                        vx: INITIAL_SPEED
                            + (xd * 0.004)
                            + self.rng.gen_range(-0.005, 0.005),
                        vy: 0.03 + self.vy + self.rng.gen_range(-0.005, 0.005),
                        t: self.rng.gen_range(0, 20),
                    });
                }
                self.last_flame_ts = self.ts;
            }
        }

        let half_gap = self.half_gap();
        self.fill_in_heights(half_gap);
        self.check_for_death(half_gap);
        self.check_for_bonus();
    }

    pub fn tunnel_heights_ptr(&self) -> *const f64 {
        self.tunnel.heights_ptr()
    }

    pub fn particle(&self, index: usize) -> Particle {
        match self.particles.get(index) {
            Some(p) => *p,
            None => Particle {
                x: -1.0,
                y: -1.0,
                vx: 0.0,
                vy: 0.0,
                t: 0,
            },
        }
    }

    pub fn num_particles(&self) -> usize {
        self.particles.len()
    }

    pub fn color(&self) -> String {
        scaled(self.x, &levels::COLOR_PROGRESSION[..]).as_css()
    }

    pub fn half_gap(&self) -> f64 {
        scaled(self.x, &levels::GAP_PROGRESSION[..])
    }

    pub fn message_text(&self) -> String {
        match self.mode {
            Mode::Intro => {
                if self.ts % 400 < 150 {
                    String::from("")
                } else {
                    String::from("Click/touch/any key to start")
                }
            }
            Mode::Running => select(self.x, &levels::MESSAGES_TEXT[..]),
            Mode::Dead => {
                let score = self.score();
                if score < 150 {
                    String::from("Bad luck - click or tap to boost!")
                } else if score > self.high_score {
                    format!("New high score! Score: {}", score)
                } else {
                    format!("Well done! Score: {}", score)
                }
            }
            Mode::ReenteringIntro => String::from(""),
        }
    }

    pub fn score(&self) -> u32 {
        self.bonus_score + ((self.x / 10.0).floor() as u32 * 10)
    }

    pub fn message_alpha(&self) -> f64 {
        match self.mode {
            Mode::Intro | Mode::ReenteringIntro => 1.0,
            Mode::Running => scaled(self.x, &levels::MESSAGES_LAYOUT[..]),
            Mode::Dead => 1.0,
        }
    }

    pub fn bonus(&self) -> Bonus {
        fn no_bonus() -> Bonus {
            Bonus {
                x: -1.0,
                relative_y: 0.0,
                y: 0.0,
                number: 0,
            }
        }
        if self.bonus_collected_at + (SHIP_DIST * 2.0) > self.x {
            no_bonus()
        } else {
            *self
                .bonuses
                .iter()
                .find(|b| {
                    b.x > self.x - BONUS_RADIUS
                        && b.x
                            < self.x
                                + SHIP_DIST
                                + BONUS_RADIUS
                                + MODEL_WIDTH as f64
                })
                .unwrap_or(&no_bonus())
        }
    }

    fn fill_in_heights(&mut self, half_gap: f64) {
        self.tunnel.fill_in_heights(half_gap, self.x);
        self.update_bonus_positions();
    }

    fn update_bonus_positions(&mut self) {
        let half_gap = self.half_gap();
        for bonus in self.bonuses.iter_mut() {
            if bonus.x - SHIP_DIST < self.tunnel.last_filled as f64
                && bonus.y < 0.0
            {
                bonus.y = self.tunnel.height_at_f(bonus.x - SHIP_DIST)
                    + (half_gap * bonus.relative_y)
            }
        }
    }

    fn speed_up(&self) -> f64 {
        scaled(self.x, &levels::SPEED_PROGRESSION[..])
    }

    fn collision_points(&self) -> [(f64, f64); 4] {
        [
            (self.x, self.y - 5.0),
            (self.x, self.y + 3.0),
            (self.x - 7.0, self.y),
            (self.x + 8.0, self.y),
        ]
    }

    fn check_for_death(&mut self, half_gap: f64) {
        let hits_tunnel = |&(x, y)| {
            let h = self.tunnel.height_at_f(x);
            y < h - half_gap || y > h + half_gap
        };

        if self.collision_points().iter().any(hits_tunnel) {
            self.dead_ts = self.ts;
            self.mode = Mode::Dead;
            for _ in 0..40 {
                let xd = self.rng.gen_range(-2.0, 2.0);
                let yd = self.rng.gen_range(-2.0, 2.0);
                self.particles.push(Particle {
                    x: self.x + xd,
                    y: self.y + yd,
                    vx: INITIAL_SPEED
                        + (xd * 0.02)
                        + self.rng.gen_range(-0.005, 0.005),
                    vy: self.vy * -0.1
                        + (yd * 0.02)
                        + self.rng.gen_range(-0.005, 0.005),
                    t: self.rng.gen_range(0, 20),
                });
            }
        }
    }

    fn check_for_bonus(&mut self) {
        let b = self.bonus();
        let hits_bonus = |&(x, y): &(f64, f64)| {
            let dx = x - b.x + SHIP_DIST;
            let dy = y - b.y;
            dx * dx + dy * dy < BONUS_RADIUS * BONUS_RADIUS
        };

        if self.collision_points().iter().any(hits_bonus) {
            self.bonus_score += 1000;
            self.bonus_collected_at = self.x;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn initial_color_and_speed() {
        let model = Model::new(12345);
        assert_eq!(model.color(), "rgb(255, 0, 0)");
        assert_eq!(model.speed_up(), 1.0);
        assert_eq!(model.half_gap(), 100.0);
    }
}
