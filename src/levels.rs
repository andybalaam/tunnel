use crate::color::Color;
use wasm_bindgen::prelude::*;

const PHASE_2: f64 = 330.0;
const PHASE_3: f64 = 890.0;
const PHASE_4: f64 = 1970.0;
const PHASE_5: f64 = 3000.0;

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub struct Bonus {
    pub x: f64,
    pub relative_y: f64,
    pub y: f64,
    pub number: u8,
}

pub fn bonuses() -> Vec<Bonus> {
    vec![
        Bonus {
            x: PHASE_2 + 190.0,
            relative_y: -0.0,
            y: -1.0,
            number: 0,
        },
        Bonus {
            x: PHASE_3,
            relative_y: -0.5,
            y: -1.0,
            number: 1,
        },
        Bonus {
            x: PHASE_4,
            relative_y: 0.5,
            y: -1.0,
            number: 2,
        },
        Bonus {
            x: PHASE_5,
            relative_y: -0.2,
            y: -1.0,
            number: 3,
        },
        Bonus {
            x: PHASE_5 + 1050.0,
            relative_y: 0.2,
            y: -1.0,
            number: 4,
        },
    ]
}

pub const COLOR_PROGRESSION: [(f64, Color); 9] = [
    (0.0, Color(255, 0, 0)),
    (PHASE_2 + 22.0, Color(255, 0, 0)),
    (PHASE_2 + 66.0, Color(0, 200, 0)),
    (PHASE_3 + 44.0, Color(0, 200, 0)),
    (PHASE_3 + 132.0, Color(0, 0, 255)),
    (PHASE_4 + 88.0, Color(0, 0, 255)),
    (PHASE_4 + 264.0, Color(255, 0, 255)),
    (PHASE_5 + 132.0, Color(255, 0, 255)),
    (PHASE_5 + 396.0, Color(255, 128, 0)),
];

pub const SPEED_PROGRESSION: [(f64, f64); 9] = [
    (0.0, 1.0),
    (PHASE_2 + 44.0, 1.0),
    (PHASE_2 + 188.0, 2.0),
    (PHASE_3 + 88.0, 2.0),
    (PHASE_3 + 376.0, 4.0),
    (PHASE_4 + 176.0, 4.0),
    (PHASE_4 + 952.0, 6.0),
    (PHASE_5 + 264.0, 6.0),
    (PHASE_5 + 2328.0, 8.0),
];

pub const GAP_PROGRESSION: [(f64, f64); 9] = [
    (0.0, 100.0),
    (PHASE_2 + 22.0, 100.0),
    (PHASE_2 + 66.0, 75.0),
    (PHASE_3 + 44.0, 75.0),
    (PHASE_3 + 132.0, 65.0),
    (PHASE_4 + 88.0, 65.0),
    (PHASE_4 + 264.0, 55.0),
    (PHASE_5 + 132.0, 55.0),
    (PHASE_5 + 396.0, 50.0),
];

pub const MESSAGES_TEXT: [(f64, &str); 14] = [
    (0.0, "Click/touch/any key to boost"),
    (110.0, ""),
    (120.0, "Phase 1"),
    (170.0, ""),
    (260.0, "Collect the bonuses!"),
    (310.0, ""),
    (PHASE_2, "Phase 2"),
    (PHASE_2 + 154.0, ""),
    (PHASE_3, "Phase 3"),
    (PHASE_3 + 302.0, ""),
    (PHASE_4, "Phase 4"),
    (PHASE_4 + 604.0, ""),
    (PHASE_5, "Phase 5 - Infinity"),
    (PHASE_5 + 1000.0, ""),
];

pub const MESSAGES_LAYOUT: [(f64, f64); 22] = [
    (52.0, 0.0),
    (70.0, 1.0),
    (88.0, 1.0),
    (110.0, 0.0),
    (120.0, 1.0),
    (148.0, 1.0),
    (170.0, 0.0),
    (260.0, 1.0),
    (288.0, 1.0),
    (310.0, 0.0),
    (PHASE_2, 1.0),
    (PHASE_2 + 132.0, 1.0),
    (PHASE_2 + 154.0, 0.0),
    (PHASE_3, 1.0),
    (PHASE_3 + 264.0, 1.0),
    (PHASE_3 + 302.0, 0.0),
    (PHASE_4, 1.0),
    (PHASE_4 + 528.0, 1.0),
    (PHASE_4 + 604.0, 0.0),
    (PHASE_5, 1.0),
    (PHASE_5 + 792.0, 1.0),
    (PHASE_5 + 1000.0, 0.0),
];
