pub trait Interpolate {
    fn interpolate(&self, sc: f64, other: &Self) -> Self;
}

impl Interpolate for u8 {
    fn interpolate(&self, sc: f64, other: &u8) -> u8 {
        (*self as f64 + (sc * (*other as f64 - *self as f64))).floor() as u8
    }
}

impl Interpolate for f64 {
    fn interpolate(&self, sc: f64, other: &f64) -> f64 {
        *self + (sc * (*other - *self))
    }
}
