use rand::rngs::StdRng;
use rand::Rng;
use rand::SeedableRng;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct Tunnel {
    seed: u32,
    rng: StdRng,
    width: usize,
    height: usize,
    ship_dist: f64,
    heights: Vec<f64>,
    pub last_filled: usize,
}

impl Tunnel {
    pub fn new(
        seed: u32,
        width: usize,
        height: usize,
        ship_dist: f64,
    ) -> Tunnel {
        let mut heights = Vec::with_capacity(width);
        heights.resize(width, 0.0);
        heights[0] = height as f64 / 2.0;

        Tunnel {
            seed,
            rng: StdRng::seed_from_u64(seed as u64),
            width,
            height,
            ship_dist,
            heights,
            last_filled: 0,
        }
    }

    pub fn reset(&mut self) {
        self.rng = StdRng::seed_from_u64(self.seed as u64);
        self.heights[0] = self.height as f64 / 2.0;
        self.last_filled = 0;
    }

    pub fn heights_ptr(&self) -> *const f64 {
        self.heights.as_ptr()
    }

    pub fn height_at_f(&self, pos_x: f64) -> f64 {
        self.heights[pos_x.floor() as usize % self.width]
    }

    fn height_at(&self, pos_x: usize) -> f64 {
        self.heights[pos_x % self.width]
    }

    fn set_height_at(&mut self, pos_x: usize, value: f64) {
        self.heights[pos_x % self.width] = value;
    }

    pub fn fill_in_heights(&mut self, half_gap: f64, x: f64) {
        let leading_edge_i =
            (x + self.width as f64 - self.ship_dist).floor() as usize;

        let mut h = self.height_at(self.last_filled);

        for i in (self.last_filled + 1)..leading_edge_i + 1 {
            let delta: f64 = self.rng.gen_range(-5.0, 5.0);
            h += delta;
            if h - half_gap < 5.0 {
                h = 5.0 + half_gap;
            } else if h + half_gap > self.height as f64 - 5.0 {
                h = self.height as f64 - (half_gap + 5.0);
            }
            self.set_height_at(i, h);
        }
        self.last_filled = leading_edge_i;
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fill_in_heights() {
        let mut tunnel = Tunnel::new(12345, 401, 300, 50.0);
        tunnel.fill_in_heights(100.0, 55.0);
        assert_eq!(tunnel.heights.len(), 401);
        assert_ne!(tunnel.heights[0], 0.0);
        assert_ne!(tunnel.heights[400], 0.0);
    }
}
