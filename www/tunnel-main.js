import { Mode, Model } from "tunnel";
import { memory } from "tunnel/tunnel_bg";

;(function() {

var model = Model.new(123456);
var prevMode = null;
var keyIsDown = false;

var canvas;
var canvasWidth = 100;
var canvasHeight = 100;

const modelWidth = model.width;
const modelHeight = model.height;
const canvasRatio = (modelWidth - 1)/modelHeight;
const shipDist = model.ship_dist;
const IMAGES = [
    loadImage("bonus1.png"),
    loadImage("bonus2.png"),
    loadImage("bonus3.png"),
    loadImage("bonus4.png"),
    loadImage("bonus5.png"),
]

function loadImage(fileName) {
    const ret = new Image(178, 177);
    ret.src = fileName;
    return ret;
}

function resize() {
    var w = window.innerWidth * 0.98;
    var h = window.innerHeight * 0.98;

    canvasWidth = w;
    canvasHeight = canvasWidth / canvasRatio;
    if (canvasHeight > h) {
        canvasHeight = h;
        canvasWidth = canvasHeight * canvasRatio;
    }
    canvas.style.marginTop = ((window.innerHeight - canvasHeight) / 2) + "px";
    canvas.style.marginLeft = ((window.innerWidth - canvasWidth) / 2) + "px";

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
}

function displayText(ctx, text, alpha) {
    var xPos = model.mode === Mode.Running ? 0.6 : 0.5;

    if (text !== "") {
        ctx.fillStyle = ((
            model.mode === Mode.Intro)
                ? 'rgba(255, 0, 0, ' + alpha + ')'
                : 'rgba(255, 255, 255, ' + alpha + ')'
        );
        ctx.fillText(
            text,
            canvasWidth * xPos,
            canvasHeight * 0.5,
            canvasWidth * 0.8
        );
    }
}

function statsEvent(metric) {
    var req = new XMLHttpRequest();
    req.open("POST", "https://smolpxl.artificialworlds.net/s/event.php");
    req.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
    req.send(metric);
}

function tick(ts) {
    window.requestAnimationFrame(tick);
    model.tick(ts, keyIsDown);

    if (prevMode === Mode.Intro && model.mode === Mode.Running) {
        statsEvent("tunnel.played");
    }
    prevMode = model.mode;

    var sc = canvasWidth / (modelWidth - 1)
    var ctx = canvas.getContext("2d");
    drawBackground(ctx);
    drawParticles(ctx, sc);
    drawTunnel(ctx, sc);
    drawShip(ctx, sc);
    drawMessage(ctx);
    drawScores(ctx);
}

function drawBackground(ctx) {
    ctx.fillStyle = 'rgba(0, 0, 0)';
    ctx.fillRect(0, 0, canvasWidth, canvasHeight);
}

function drawTunnel(ctx, sc) {
    var offset = model.x - Math.floor(model.x);
    var half_gap = model.half_gap();

    ctx.fillStyle = model.color();
    const heightsPtr = model.tunnel_heights_ptr();
    const heights = new Float64Array(memory.buffer, heightsPtr, modelWidth);

    // Draw the tunnel
    for (var i = 0; i < modelWidth; ++i) {
        var hi = Math.floor(1 + model.x + i - shipDist)
        var h = heights[hi % modelWidth];
        var x = (i - offset) * sc;
        var w = sc + 1;
        var topOfGap = (h - half_gap) * sc;
        var bottomOfGap = (h + half_gap) * sc;
        ctx.fillRect(x, 0, w, topOfGap);
        ctx.fillRect(x, bottomOfGap, w, canvasHeight - bottomOfGap);
    }

    // Draw the bonus token
    if (model.mode === Mode.Running) {
        var b = model.bonus();
        if (b.x > 0.0) {
            const w = 10 * sc;
            ctx.drawImage(
                IMAGES[b.number],
                ((b.x - model.x) * sc) - w,
                (b.y * sc) - w,
                w * 2,
                w * 2
            );
        }
    }
}

function drawParticles(ctx, sc) {
    const numParticles = model.num_particles();
    for (var i = 0; i < numParticles; ++i) {
        var p = model.particle(i);
        ctx.fillStyle = p.color();
        rect(ctx, sc, shipDist + p.x - model.x, p.y, shipDist + p.x - model.x, p.y);
    }
}

function drawShip(ctx, sc) {
    if (model.mode !== Mode.Running) {
        return;
    }
    const d = shipDist;
    ctx.fillStyle = 'rgba(212, 212, 212, 255)';
    rect(ctx, sc, d - 1, model.y - 5, d + 2, model.y - 5);
    rect(ctx, sc, d - 3, model.y - 4, d + 4, model.y - 3);

    ctx.fillStyle = 'rgba(229, 32, 251, 255)';
    rect(ctx, sc, d - 6, model.y + 1, d + 7, model.y + 1);
    rect(ctx, sc, d - 4, model.y + 2, d + 5, model.y + 2);
    rect(ctx, sc, d - 2, model.y + 3, d + 3, model.y + 3);

    ctx.fillStyle = 'rgba(19, 239, 11, 255)';
    rect(ctx, sc, d - 2, model.y - 2, d + 3, model.y - 2);
    rect(ctx, sc, d - 6, model.y - 1, d + 7, model.y - 1);
    rect(ctx, sc, d - 7, model.y, d + 8, model.y);

    ctx.fillStyle = 'rgba(237, 122, 83, 255)';
    rect(ctx, sc, d - 4, model.y - 2, d - 3, model.y - 1);
    rect(ctx, sc, d, model.y - 2, d + 1, model.y - 1);
    rect(ctx, sc, d + 4, model.y - 2, d + 5, model.y - 1);
}

function drawMessage(ctx) {
    var txt = model.message_text();
    if (txt !== "") {
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.font = 'small-caps ' + (canvasWidth * 0.05) + 'px sans'
        displayText(ctx, model.message_text(), model.message_alpha());
    }
}

function drawScores(ctx) {
    ctx.fillStyle = 'rgba(255, 255, 255, 0.9)';
    if (model.mode === Mode.Running || model.mode === Mode.Dead) {
        ctx.fillText(
            'Score: ' + model.score(),
            canvasWidth * 0.2,
            canvasHeight * 0.09,
            canvasWidth * 0.4
        );
    }
    ctx.fillText(
        'High: ' + model.high_score,
        canvasWidth * 0.8,
        canvasHeight * 0.09,
        canvasWidth * 0.4
    );
}

function rect(ctx, sc, x1, y1, x2, y2) {
    ctx.fillRect(
        (x1 - 0.5) * sc,
        (y1 - 0.5) * sc,
        ((1 + x2 - x1) * sc) + 1,
        ((1 + y2 - y1) * sc) + 1
    );
}

function onKeyDown(e) {
    keyIsDown = true;
    e.preventDefault();
}

function onKeyUp(e) {
    keyIsDown = false;
    e.preventDefault();
}

function start() {
    canvas = document.getElementById("canvas");
    document.addEventListener("touchstart", onKeyDown);
    document.addEventListener("keydown", onKeyDown);
    document.addEventListener("mousedown", onKeyDown);
    document.addEventListener("touchend", onKeyUp);
    document.addEventListener("touchcancel", onKeyUp);
    document.addEventListener("keyup", onKeyUp);
    document.addEventListener("mouseup", onKeyUp);
    resize();
    window.requestAnimationFrame(tick);
}

window.onresize = resize;

start();

}())
