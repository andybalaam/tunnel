# tunnel

A simple web game built in Rust, WebAssembly and JavaScript.

Play: https://andybalaam.gitlab.io/tunnel/

![](images-src/tunnel.png)

## Prerequisites

[Rust, wasm-pack, and
npm](https://rustwasm.github.io/book/game-of-life/setup.html)

## Build

```bash
make
```

Now `pkg/` contains the compiled wasm module "tunnel".

That module is listed as a dependency in [www/package.json](www/package.json)
and then imported in [www/tunnel-main.js](www/tunnel-main.js).

## Run

```bash
make run
```

This will build the JavaScript and launch a development server that rebuilds
on demand.

Visit http://localhost:8080/ to see it running.

Note that you can run `make` in a separate terminal and your newly-compiled
wasm will be immediately updated.

## Deploy

```bash
make dist
```

Now `public/` contains the full code to upload to somewhere.

## Code

The rendering code is JavaScript in [www/tunnel-main.js](www/tunnel-main.js).

The game engine is Rust in [src/lib.rs](src/lib.rs).

## License

Copyright 2019 Andy Balaam, released under APGLv3 or later.  See
[LICENSE](LICENSE) for details.
