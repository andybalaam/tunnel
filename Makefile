all: test

compile:
	cargo fmt
	wasm-pack build

test: compile
	cargo test
	wasm-pack test --firefox --headless

run: compile
	cd www && npm install && npm run start

dist: test
	cd www && npm install && npm run-script build
